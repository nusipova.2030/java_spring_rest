package fari.iitu.ead.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class RegisterRequest {
    @NotBlank
    private String login;

    @NotBlank
    private String password;
}
