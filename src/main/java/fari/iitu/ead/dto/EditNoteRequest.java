package fari.iitu.ead.dto;

import lombok.Data;

@Data
public class EditNoteRequest {

    private String name;
    private String description;
}
