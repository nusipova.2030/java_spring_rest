package fari.iitu.ead.repository;

import fari.iitu.ead.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User , Long> {
    boolean existsByLogin(String login);
    User findByLogin(String login);
    User findByToken(Integer token);
}
