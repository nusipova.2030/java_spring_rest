package fari.iitu.ead.service.note;

import fari.iitu.ead.dto.AddNoteRequest;
import fari.iitu.ead.dto.EditNoteRequest;
import fari.iitu.ead.dto.UserNotesResponse;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface NoteService {
    ResponseEntity<?> addNote(AddNoteRequest request);
    List<UserNotesResponse> getAllUserNotes(int token);
    ResponseEntity<?> editNote(int token , Long id , EditNoteRequest request);
    ResponseEntity<?> deleteNote(int token , Long id);
}
