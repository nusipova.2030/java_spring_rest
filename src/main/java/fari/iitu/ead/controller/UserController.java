package fari.iitu.ead.controller;

import fari.iitu.ead.dto.*;
import fari.iitu.ead.service.note.NoteService;
import fari.iitu.ead.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    private final UserService userService;
    private final NoteService noteService;

    @Autowired
    public UserController(@Lazy UserService userService, NoteService noteService) {
        this.userService = userService;
        this.noteService = noteService;
    }

    @PostMapping(value = "/register")
    public ResponseEntity<?> register(@RequestBody RegisterRequest request){
        return userService.register(request);
    }
    @PostMapping(value = "/login")
    public ResponseEntity<?> login(@RequestBody LoginRequest request){
        return userService.login(request);
    }

    @PostMapping(value = "/note/add")
    public ResponseEntity<?> addNote(@RequestBody AddNoteRequest request){
        return noteService.addNote(request);
    }

    @GetMapping(value = "/notes")
    public List<UserNotesResponse> notes(@RequestParam(name = "token")int token){
        return noteService.getAllUserNotes(token);
    }

    @PutMapping(value = "/edit/note")
    public ResponseEntity<?> editNote(@RequestParam(name = "id")Long id,
                                      @RequestParam(name = "token")int token,
                                      @RequestBody EditNoteRequest request){
        return noteService.editNote(token , id , request);
    }

    @DeleteMapping(value = "/delete/note")
    public ResponseEntity<?> deleteNote(@RequestParam(name = "id")Long id,
                                        @RequestParam(name = "token")int token){
        return noteService.deleteNote(token, id);
    }
}
