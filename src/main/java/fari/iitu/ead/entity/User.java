package fari.iitu.ead.entity;

import fari.iitu.ead.dto.RegisterRequest;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "users")
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "fake_token" , nullable = true)
    private Integer token;

    public User(RegisterRequest request) {
        this.login = request.getLogin();
        this.password = request.getPassword();
    }

    public User(String login, String password, int token) {
        this.login = login;
        this.password = password;
        this.token = token;
    }

    public User() {
    }
}
