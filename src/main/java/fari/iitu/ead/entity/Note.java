package fari.iitu.ead.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "notes")
@Data
public class Note {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description" ,  columnDefinition = "TEXT")
    private String description;

    @ManyToOne
    private User user;


}
